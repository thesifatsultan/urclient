﻿using EasyModbus;
using System;

namespace URClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ModbusClient modbusClient = new ModbusClient("192.168.110.129", 502);    //Ip-Address and Port of Modbus-TCP-Server
            modbusClient.Connect();                                                    //Connect to Server
                                                                                       //modbusClient.WriteMultipleCoils(4, new bool[] { true, true, true, true, true, true, true, true, true, true });    //Write Coils starting with Address 5                                                                                       //bool[] readCoils = modbusClient.ReadCoils(1, 1);                        //Read 10 Coils from Server, starting with address 10
            #region Chart
            //0 Inputs, bits 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            //1 Outputs, bits 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            //2 SetOutputsBitsMask 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            //3 ClearOutputsBitsMask 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            
            //4 Analog input 0(0 - 65535)
            //5 Analog input 0 domain e { 0 = current[mA], 1 = voltage[mV]}
            //6 Analog input 1(0 - 65535)
            //7 Analog input 1 domain e { 0 = current[mA], 1 = voltage[mV]}
            //8 Analog input 2(tool)(0 - 65535)
            //9 Analog input 2(tool) domain e { 0 = current[mA], 1 = voltage[mV]}
            //10 Analog input 3(tool)(0 - 65535)
            //11 Analog input 3(tool) range e { 0 = current[mA], 1 = voltage[mV]}
            //16 Analog output 0 output(0 - 65535)
            //17 Analog output 0 output domain e { 0 = current[mA], 1 = voltage[mV]}
            //18 Analog output 1 output(0 - 65535)
            //19 Analog output 1 output domain { 0 = current[mA], 1 = voltage[mV]}
            
            //20 Tool output voltage (V)e { 0V, 12V, 24V}
            //21 Tool digital input bits
            //22 Tool digital output bits
            
            //24 Euromap67 input bits (0 - 15)
            //25 Euromap67 input bits(16 - 32)
            //26 Euromap67 output bits(0 - 15)(read only!)
            //27 Euromap67 output bits(16 - 32)(read only!)
            //28 Euromap 24V voltage
            //29 Euromap 24V current
            
            //30 Configurable inputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            //31 Configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            //32 Bit mask configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            //33 Clear configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box

            //270 Base joint angle (in mrad)	 	 
            //271 Shoulder joint angle (in mrad)	 	 
            //272 Elbow joint angle (in mrad)	 	 
            //273 Wrist1 joint angle (in mrad)	 	 
            //274 Wrist2 joint angle (in mrad)	 	 
            //275 Wrist3 joint angle (in mrad)	 	 

            //280 Base joint angle velocity(in mrad / s)
            //281 Shoulder joint angle velocity(in mrad / s)
            //282 Elbow joint angle velocity(in mrad / s)
            //283 Wrist1 joint angle velocity(in mrad / s)
            //284 Wrist2 joint angle velocity(in mrad / s)
            //285 Wrist3 joint angle velocity(in mrad / s)

            //290 Base joint current (in mA)	 	 
            //291 Shoulder joint current (in mA)	 	 
            //292 Elbow joint current (in mA)	 	 
            //293 Wrist1 joint current (in mA)	 	 
            //294 Wrist2 joint current (in mA)	 	 
            //295 Wrist3 joint current (in mA)	 	 

            //300 Base joint temperature (in C)	 	 
            //301 Shoulder joint temperature (in C)	 
            //302 Elbow joint temperature (in C)	 
            //303 Wrist1 joint temperature (in C)	 
            //304 Wrist2 joint temperature (in C)	 
            //305 Wrist3 joint temperature (in C)
            #endregion

            #region I/O
            //1 Outputs, bits 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            //16 Analog output 0 output(0 - 65535)
            //17 Analog output 0 output domain e { 0 = current[mA], 1 = voltage[mV]}
            //18 Analog output 1 output(0 - 65535)
            //19 Analog output 1 output domain { 0 = current[mA], 1 = voltage[mV]}
            //20 Tool output voltage (V)e { 0V, 12V, 24V}
            //22 Tool digital output bits
            //26 Euromap67 output bits(0 - 15)(read only!)
            //27 Euromap67 output bits(16 - 32)(read only!)
            //28 Euromap 24V voltage
            //29 Euromap 24V current
            //31 Configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            //32 Bit mask configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box

            #region 1 Outputs, bits 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box
            int[] Outputs = modbusClient.ReadHoldingRegisters(1, 1);
            Console.WriteLine("Outputs, bits 0 - 15[BBBBBBBBTTxxxxxx] x = undef, T = tool, B = box: " + Outputs[0]);

            #endregion

            #region 16, 17 Analog output 0
            int[] AnalogOutput0Domain = modbusClient.ReadHoldingRegisters(16, 1);
            int[] AnalogOutput0Value = modbusClient.ReadHoldingRegisters(17, 1);

            switch (AnalogOutput0Domain[0])
            {
                case 0:
                    Console.WriteLine("Analog Output 0 Current [mA]: " +AnalogOutput0Value[0]);
                    break;

                case 1:
                    Console.WriteLine("Analog Output 0 Voltage [mV]: " + AnalogOutput0Value[0]);
                    break;
                default:
                    break;
            }
            #endregion

            #region 18, 19 Analog output 1
            int[] AnalogOutput1Domain = modbusClient.ReadHoldingRegisters(16, 1);
            int[] AnalogOutput1Value = modbusClient.ReadHoldingRegisters(17, 1);

            switch (AnalogOutput1Domain[0])
            {
                case 0:
                    Console.WriteLine("Analog Output 1 Current [mA]: " + AnalogOutput1Value[0]);
                    break;

                case 1:
                    Console.WriteLine("Analog Output 1 Voltage [mV]: " + AnalogOutput1Value[0]);
                    break;
                default:
                    break;
            }
            #endregion

            #region 20 Tool output voltage (V)e { 0V, 12V, 24V}
            int[] ToolOutputVoltage = modbusClient.ReadHoldingRegisters(20, 1);
            Console.WriteLine("20 Tool output voltage (V)e { 0V, 12V, 24V }  : " + ToolOutputVoltage[0]);
            #endregion

            #region 22 Tool digital output bits
            int[] ToolDigitalOutputBits = modbusClient.ReadHoldingRegisters(22, 1);
            Console.WriteLine("22 Tool digital output bits: " + ToolDigitalOutputBits[0]);

            #endregion

            #region 26 Euromap67 output bits(0 - 15)(read only!)
            #endregion

            #region 27 Euromap67 output bits(16 - 32)(read only!)
            #endregion

            #region 28 Euromap 24V voltage
            #endregion

            #region 29 Euromap 24V current
            #endregion

            #region 31 Configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            #endregion

            #region 32 Bit mask configurable outputs, bits[BBBBBBBBxxxxxxxx] x = undef, T = tool, B = box
            #endregion

            Console.WriteLine("\n");

            #endregion

            #region ROBOT STATE
            int[] robotMode = modbusClient.ReadHoldingRegisters(258, 1);                        //Read 1 Coil from Server, starting with address 258
            for (int i = 0; i < robotMode.Length; i++)
            {
                switch (robotMode[i])
                {
                    case 0:
                        Console.Write("Robot is Disconnected\n");
                        break;
                    case 1:
                        Console.Write("Robot is Confirm_safety\n");
                        break;
                    case 2:
                        Console.Write("Robot is Booting\n");
                        break;
                    case 3:
                        Console.Write("Robot is Power_off\n");
                        break;
                    case 4:
                        Console.Write("Robot is Power_on\n");
                        break;
                    case 5:
                        Console.Write("Robot is Idle\n");
                        break;
                    case 6:
                        Console.Write("Robot is Backdrive\n");
                        break;
                    case 7:
                        Console.Write("Robot is Running\n");
                        break;
                    default:
                        Console.WriteLine("Dead Robot\n");
                        break;
                }
            }

            Console.WriteLine("\n");
            #endregion

            #region BASE JOINT, SHOULDER JOINT, ELBOW JOINT, WRIST1, WRIST2, WRIST3 SENSOR VALUES

            #region ANGLE VALUES

            Console.WriteLine("***ANGLE VALUES****");

            #region Base joint angle (in mrad) {270}
            int[] BaseJoingAngle = modbusClient.ReadHoldingRegisters(270, 1);
            //for (int i = 0; i < BaseJoingAngle.Length; i++) { Console.WriteLine("Base Joint Angle (mrad): " + BaseJoingAngle[i]); }
            for (int i = 0; i < BaseJoingAngle.Length; i++) { Console.WriteLine("Base Joint Angle (degree): " + ((((BaseJoingAngle[i] * 180) / Math.PI) / 1000)-360)); }
            //for (int i = 0; i < BaseJoingAngle.Length; i++) { Console.WriteLine("Base Joint Angle (degree): " + ((BaseJoingAngle[i]*180)/Math.PI)/1000); }
            #endregion

            #region Shoulder joint angle {271}
            int[] ShoulderJointAngle = modbusClient.ReadHoldingRegisters(271, 1);
            //for (int i = 0; i < ShoulderJointAngle.Length; i++) { Console.WriteLine("Shoulder Joint Angle (mrad) : " + ShoulderJointAngle[i]); }
            for (int i = 0; i < ShoulderJointAngle.Length; i++) { Console.WriteLine("Shoulder Joint Angle (degree): " + ((((ShoulderJointAngle[i] * 180) / Math.PI) / 1000)-360)) ; }
            //for (int i = 0; i < ShoulderJointAngle.Length; i++) { Console.WriteLine("Shoulder Joint Angle (degree): " + ((ShoulderJointAngle[i]*180)/Math.PI)/1000); }
            #endregion

            #region Elbow joint angle (in mrad)	 {272}
            int[] ElbowJointAngle = modbusClient.ReadHoldingRegisters(272, 1);
            //for (int i = 0; i < ElbowJointAngle.Length; i++) { Console.WriteLine("Elbow Joint Angle (mrad): " + ElbowJointAngle[i]); }
            for (int i = 0; i < ElbowJointAngle.Length; i++) { Console.WriteLine("Elbow Joint Angle (degree): " + ((((ElbowJointAngle[i] * 180) / Math.PI) / 1000)-360)); }
            //for (int i = 0; i < ElbowJointAngle.Length; i++) { Console.WriteLine("Elbow Joint Angle (degree): " + ((ElbowJointAngle[i]*180)/Math.PI)/1000); }
            #endregion

            #region Wrist1 joint angle (in mrad) {273}
            int[] Wrist1JointAngle = modbusClient.ReadHoldingRegisters(273, 1);
            //for (int i = 0; i < Wrist1JointAngle.Length; i++) { Console.WriteLine("Wrist1 joint angle (mrad): " + Wrist1JointAngle[i]); }
            for (int i = 0; i < Wrist1JointAngle.Length; i++) { Console.WriteLine("Wrist1 joint angle (degree): " + ((((Wrist1JointAngle[i] * 180) / Math.PI) / 1000)-360)); }
            //for (int i = 0; i < Wrist1JointAngle.Length; i++) { Console.WriteLine("Wrist1 joint angle (degree): " + ((Wrist1JointAngle[i]*180)/Math.PI)/1000); }
            #endregion

            #region Wrist2 joint angle (in mrad) {274}
            int[] Wrist2JointAngle = modbusClient.ReadHoldingRegisters(274, 1);
            //for (int i = 0; i < Wrist2JointAngle.Length; i++) { Console.WriteLine("Wrist2 joint angle (mrad) : " + Wrist2JointAngle[i]); }
            for (int i = 0; i < Wrist2JointAngle.Length; i++) { Console.WriteLine("Wrist2 joint angle (degree) : " + ((((Wrist2JointAngle[i] * 180) / Math.PI) / 1000)-360)); }
            //for (int i = 0; i < Wrist2JointAngle.Length; i++) { Console.WriteLine("Wrist2 joint angle (degree) : " + ((Wrist2JointAngle[i]*180)/Math.PI)/1000); }
            #endregion

            #region Wrist3 joint angle (in mrad) {275}
            int[] Wrist3JointAngle = modbusClient.ReadHoldingRegisters(275, 1);
            //for (int i = 0; i < Wrist3JointAngle.Length; i++) { Console.WriteLine("Wrist3 joint angle (mrad): " + Wrist3JointAngle[i]); }
            for (int i = 0; i < Wrist3JointAngle.Length; i++) { Console.WriteLine("Wrist3 joint angle (degree): " + ((((Wrist3JointAngle[i] * 180) / Math.PI) / 1000)-360)); }
            //for (int i = 0; i < Wrist3JointAngle.Length; i++) { Console.WriteLine("Wrist3 joint angle (degree): " + ((Wrist3JointAngle[i]*180)/Math.PI)/1000); }
            #endregion

            Console.WriteLine("\n");

            #endregion

            #region CURRENT VALUES

            Console.WriteLine("***CURRENT VALUES****");

            #region Base joint current (in mA) {290}
            int[] BaseJointCurrent = modbusClient.ReadHoldingRegisters(290, 1);
            for (int i = 0; i < BaseJointCurrent.Length; i++) { Console.WriteLine("Base Joint Current : " + BaseJointCurrent[i]/1000); }
            #endregion

            #region Shoulder joint current (in mA) {291} 
            int[] ShoulderJointCurrent = modbusClient.ReadHoldingRegisters(291, 1);
            for (int i = 0; i < ShoulderJointCurrent.Length; i++) { Console.WriteLine("Shoulder joint current : " + ShoulderJointCurrent[i]/1000); }
            #endregion

            #region Elbow joint current (in mA) {292}
            int[] ElbowJointCurrent = modbusClient.ReadHoldingRegisters(292, 1);
            for (int i = 0; i < ElbowJointCurrent.Length; i++) { Console.WriteLine("Elbow joint current : " + ElbowJointCurrent[i]/1000); }
            #endregion

            #region Wrist1 joint current (in mA) {293} 
            int[] Wrist1JointCurrent = modbusClient.ReadHoldingRegisters(293, 1);
            for (int i = 0; i < Wrist1JointCurrent.Length; i++) { Console.WriteLine("Wrist1 joint current : " + Wrist1JointCurrent[i]/1000); }
            #endregion

            #region Wrist2 joint current (in mA) {294}	 
            int[] Wrist2JointCurrent = modbusClient.ReadHoldingRegisters(294, 1);
            for (int i = 0; i < Wrist2JointCurrent.Length; i++) { Console.WriteLine("Wrist2 joint current : " + Wrist2JointCurrent[i]); }
            #endregion

            #region Wrist3 joint current (in mA) {295}	
            int[] Wrist3JointCurrent = modbusClient.ReadHoldingRegisters(295, 1);
            for (int i = 0; i < Wrist3JointCurrent.Length; i++) { Console.WriteLine("Wrist3 joint current : " + Wrist3JointCurrent[i]); }
            #endregion

            Console.WriteLine("\n");

            #endregion

            #region TEMPERATURE VALUES

            Console.WriteLine("***TEMPERATURE VALUES****");

            #region Base joint temperature (in C) {300}
            int[] BaseJointTemperature = modbusClient.ReadHoldingRegisters(300, 1);
            for (int i = 0; i < BaseJointTemperature.Length; i++) { Console.WriteLine("Base joint temperature : " + BaseJointTemperature[i]); }
            #endregion

            #region Shoulder joint temperature (in C) {301}
            int[] ShoulderJointTemperature = modbusClient.ReadHoldingRegisters(301, 1);
            for (int i = 0; i < ShoulderJointTemperature.Length; i++) { Console.WriteLine("Shoulder Joint Current : " + ShoulderJointTemperature[i]); }
            #endregion

            #region Elbow joint temperature (in C) {302}
            int[] ElbowJointTemperature = modbusClient.ReadHoldingRegisters(302, 1);
            for (int i = 0; i < ElbowJointTemperature.Length; i++) { Console.WriteLine("Elbow Joint Current : " + ElbowJointTemperature[i]); }
            #endregion

            #region Wrist1 joint temperature (in C)	{303}
            int[] Wrist1JointTemperature = modbusClient.ReadHoldingRegisters(303, 1);
            for (int i = 0; i < Wrist1JointTemperature.Length; i++) { Console.WriteLine("Wrist1 Joint Current : " + Wrist1JointTemperature[i]); }
            #endregion

            #region Wrist2 joint temperature (in C)	{304}
            int[] Wrist2JointTemperature = modbusClient.ReadHoldingRegisters(304, 1);
            for (int i = 0; i < Wrist2JointTemperature.Length; i++) { Console.WriteLine("Wrist2 Joint Current : " + Wrist2JointTemperature[i]); }
            #endregion

            #region Wrist3 joint temperature (in C)	{305}
            int[] Wrist3JointTemperature = modbusClient.ReadHoldingRegisters(305, 1);
            for (int i = 0; i < Wrist3JointTemperature.Length; i++) { Console.WriteLine("Wrist3 Joint Current : " + Wrist3JointTemperature[i]); }
            #endregion

            Console.WriteLine("\n");

            #endregion

            #region ANGLE VELOCITY VALUES

            Console.WriteLine("***ANGLE VELOCITY VALUES****");

            #region Base joint angle velocity(in mrad / s) {280}
            int[] BaseJointAngleVelocity = modbusClient.ReadHoldingRegisters(280, 1);
            for (int i = 0; i < BaseJointAngleVelocity.Length; i++) { Console.WriteLine("Base joint angle velocity : " + BaseJointAngleVelocity[i]); }
            #endregion

            #region Shoulder joint angle velocity(in mrad / s) {281}
            int[] ShoulderJointAngleVelocity = modbusClient.ReadHoldingRegisters(281, 1);
            for (int i = 0; i < ShoulderJointAngleVelocity.Length; i++) { Console.WriteLine("Shoulder joint angle velocity : " + ShoulderJointAngleVelocity[i]); }
            #endregion

            #region Elbow joint angle velocity(in mrad / s) {282}
            int[] ElbowJointAngleVelocity = modbusClient.ReadHoldingRegisters(282, 1);
            for (int i = 0; i < ElbowJointAngleVelocity.Length; i++) { Console.WriteLine("Elbow joint angle velocity : " + ElbowJointAngleVelocity[i]); }
            #endregion

            #region Wrist1 joint angle velocity(in mrad / s) {283}
            int[] Wrist1JointAngleVelocity = modbusClient.ReadHoldingRegisters(283, 1);
            for (int i = 0; i < Wrist1JointAngleVelocity.Length; i++) { Console.WriteLine("Wrist1 joint angle velocity : " + Wrist1JointAngleVelocity[i]); }
            #endregion

            #region Wrist2 joint angle velocity(in mrad / s) {284}
            int[] Wrist2JointAngleVelocity = modbusClient.ReadHoldingRegisters(284, 1);
            for (int i = 0; i < Wrist2JointAngleVelocity.Length; i++) { Console.WriteLine("Wrist2 joint angle velocity : " + Wrist2JointAngleVelocity[i]); }
            #endregion

            #region Wrist3 joint angle velocity(in mrad / s) {285}
            int[] Wrist3JointAngleVelocity = modbusClient.ReadHoldingRegisters(285, 1);
            for (int i = 0; i < Wrist3JointAngleVelocity.Length; i++) { Console.WriteLine("Wrist3 joint angle velocity : " + Wrist3JointAngleVelocity[i]); }
            #endregion

            Console.WriteLine("\n");

            #endregion

            Console.WriteLine("\n");

            #endregion


            modbusClient.Disconnect();                                                //Disconnect from Server
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
